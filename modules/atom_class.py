import numpy as np

class Atom:
    # constructor

    def __init__(self, type='C', coords=[.0, .0, .0]):
        self.type = type
        self.coords = np.array(coords)
    
    # methods

    def print(self):
        print('{0}:\t{1}'.format(self.type, self.coords))

