'''
Author: Tom Rodemund
Date: 26.10.2020

Contains the DFTB parameters to construct the hamiltonians and overlap matrices
'''

import numpy as np
import scipy.constants as sc

from atom_class import Atom
from constants import CUTOFF

# bohr radius in [m] to [AA]
a0 = sc.physical_constants['Bohr radius'][0] * 10**10
# print('a0 = {0} AA'.format(a0))
# hartree energy in eV
Eh = sc.physical_constants['Hartree energy in eV'][0]
# print('Eh = {0} eV'.format(Eh))

class ParameterSet:

    def __init__(self, targetdir):
        '''
        Initializing and reading all skf-files to store the
        parameter set for when it's needed.

        targetdir.. path to hamiltonian-files
        '''
        # columns per hamiltonian/overlap
        ncolumns = 10

        #######################################
        # load C-C-interaction
        #######################################
        
        C_C_filename = 'C-C.skf'

        f = open(targetdir + C_C_filename, 'r')


        # onsite-energies

        line = f.readline().split('\t')
        # print(line)

        self.C_E_p = float(line[1]) * Eh
        self.C_E_s = float(line[2]) * Eh


        # grid parameters

        line = f.readline().split('\t')
        f.close()

        self.C_dx = float(line[0]) * a0 # dist in bohr, converted to AA
        self.C_n_gridpoints = int(line[1])
        self.C_maxdist = self.C_dx * self.C_n_gridpoints


        # read integrals

        # all orbital interactions
        data = np.loadtxt(targetdir + C_C_filename, skiprows=2).T


        # hamiltonian

        self.C_ham_pp_sigma = data[5] * Eh
        self.C_ham_pp_pi = data[6] * Eh
        self.C_ham_sp_sigma = data[8] * Eh
        self.C_ham_ss_sigma = data[9] * Eh

        # overlap

        self.C_ol_pp_sigma = data[ncolumns+5]
        self.C_ol_pp_pi = data[ncolumns+6]
        self.C_ol_sp_sigma = data[ncolumns+8]
        self.C_ol_ss_sigma = data[ncolumns+9]

        if len(self.C_ham_ss_sigma) != self.C_n_gridpoints:
            print('WARNING: n_gridpoints issue (C-C)')
        

        #######################################
        # load H-C-interaction
        #######################################

        HC_filename = 'H-C.skf'

        f = open(targetdir + HC_filename, 'r')

        # grid parameters

        line = f.readline().split('\t')
        f.close()

        self.HC_dx = float(line[0]) * a0 # dist in bohr, converted to AA
        self.HC_n_gridpoints = int(line[1])
        self.HC_maxdist = self.HC_dx * self.HC_n_gridpoints


        # read integrals

        # all orbital interactions
        data = np.loadtxt(targetdir + HC_filename, skiprows=1).T


        '''
        ATTENTION: values are saved for < s_H | p_C_i > and so on
        '''

        # hamiltonian

        self.HC_ham_sp_sigma = data[8] * Eh
        self.HC_ham_ss_sigma = data[9] * Eh

        # overlap
 
        self.HC_ol_sp_sigma = data[ncolumns+8]
        self.HC_ol_ss_sigma = data[ncolumns+9]

        if len(self.HC_ham_ss_sigma) != self.HC_n_gridpoints:
            print('WARNING: n_gridpoints issue (H-C)')

        #######################################
        # load H-H-interaction
        #######################################

        
        H_H_filename = 'H-H.skf'

        f = open(targetdir + H_H_filename, 'r')


        # onsite-energies

        line = f.readline().split('\t')
        # print(line)

        self.H_E_s = float(line[2]) * Eh


        # grid parameters

        line = f.readline().split('\t')
        f.close()

        self.H_dx = float(line[0]) * a0 # dist in bohr, converted to AA
        self.H_n_gridpoints = int(line[1])
        self.H_maxdist = self.H_dx * self.H_n_gridpoints


        # read integrals

        # all orbital interactions
        data = np.loadtxt(targetdir + H_H_filename, skiprows=2).T


        # hamiltonian

        self.H_ham_ss_sigma = data[9] * Eh

        # overlap
 
        self.H_ol_ss_sigma = data[ncolumns+9]

        if len(self.H_ham_ss_sigma) != self.H_n_gridpoints:
            print('WARNING: n_gridpoints issue (H-H)')


    #######################################
    # interaction between atoms
    #######################################

    def get_interaction(self, atom_1, atom_2):
        '''
        Gives interaction and overlap of orbitals 2s, px, py, pz between to atoms
        via a basis transformation.
        '''


        # print('\n\npair:')
        # atom_1.print()
        # atom_2.print()



        r = atom_2.coords - atom_1.coords
        d = np.linalg.norm(r)

        # print('d = {0}'.format(d))

        # for directional cosines
        r = r / d

        if atom_1.type == 'C' and atom_2.type == 'C':
            if d > CUTOFF:
                return [np.zeros((4,4)), np.zeros((4,4))]

            if d > self.C_maxdist:
                print('WARNING: distance between atoms longer than covered by DFTB parameters')
                return [np.zeros((4,4)), np.zeros((4,4))]

            # calculating where to look for right interactions
            i_loc = d / self.C_dx
            i_high = int(i_loc)
            i_low = i_high - 1

            # factors for linear interpolation between gridpoints

            factor_low = i_loc - i_low
            factor_high = 1. - factor_low

            # grabbing the right values

            interaction_table = []

            for table in [
                self.C_ham_pp_sigma, self.C_ham_pp_pi, self.C_ham_sp_sigma, self.C_ham_ss_sigma,
                self.C_ol_pp_sigma, self.C_ol_pp_pi, self.C_ol_sp_sigma, self.C_ol_ss_sigma
                ]:

                interaction = table[i_low] * factor_low + table[i_high] * factor_high
                
                interaction_table.append(interaction)
            
            interaction_table = np.array(interaction_table)

            # print('H: {0}'.format(interaction_table[:4]))
            # print('S: {0}'.format(interaction_table[4:]))
            
            # hamiltonian
            H_pp_sigma, H_pp_pi, H_sp_sigma, H_ss_sigma = interaction_table[:4]
            # overlap
            S_pp_sigma, S_pp_pi, S_sp_sigma, S_ss_sigma = interaction_table[4:]

            # calculating interaction in fixed coordinate system

            '''
            we need (s_A, s_B), (s_A, px_B), (s_A, py_B), (s_A, pz_B), (px_A, px)

            first orbital is of atom 1, second of atom 2
            '''

            # H

            # s pi orbitals
            table_sp = np.array([c * H_sp_sigma for c in r])
            # pi pj orbitals
            table_pp = np.array([[c_i * c_j * (H_pp_sigma-H_pp_pi) for c_j in r] for c_i in r]) + np.identity(3) * H_pp_pi
            # construction of H
            H = np.concatenate(([table_sp], table_pp), axis=0)
            table_addon = np.transpose([np.concatenate(([H_ss_sigma], -table_sp))])
            H = np.concatenate((table_addon, H), axis=1)


            # S

            # s pi orbitals
            table_sp = np.array([c * S_sp_sigma for c in r])
            # pi pj orbitals
            table_pp = np.array([[c_i * c_j * (S_pp_sigma-S_pp_pi) for c_j in r] for c_i in r]) + np.identity(3) * S_pp_pi
            # construction of S
            S = np.concatenate(([table_sp], table_pp), axis=0)
            table_addon = np.transpose([np.concatenate(([S_ss_sigma], -table_sp))])
            S = np.concatenate((table_addon, S), axis=1)


            return [H, S]

        elif atom_1.type == 'H' and atom_2.type == 'H':
            if d > CUTOFF:
                return [np.zeros((1,1)), np.zeros((1,1))]

            if d > self.C_maxdist:
                print('WARNING: distance between atoms longer than covered by DFTB parameters')
                return [np.zeros((1,1)), np.zeros((1,1))]

            # calculating where to look for right interactions
            i_loc = d / self.H_dx
            i_high = int(i_loc)
            i_low = i_high - 1

            # print('i_low = {0}'.format(i_low))

            # factors for linear interpolation between gridpoints

            factor_low = i_loc - i_low
            factor_high = 1. - factor_low

            # grabbing the right values
            
            # hamiltonian
            H_ss_sigma = self.H_ham_ss_sigma[i_low] * factor_low + self.H_ham_ss_sigma[i_high] * factor_high
            # overlap
            S_ss_sigma = self.H_ol_ss_sigma[i_low] * factor_low + self.H_ol_ss_sigma[i_high] * factor_high


            # H

            # needs to be converted to numpy array,
            # otherwise np.block later wont work
            # (dont ask me why)
            H = np.array([[H_ss_sigma]])

            # S

            S = np.array([[S_ss_sigma]])


            return [H, S]

        elif (atom_1.type == 'C' and atom_2.type == 'H') or (atom_1.type == 'H' and atom_2.type == 'C'):
            '''
            constructing HC case, as entries are saved that way. if HC case is needed matrices will be transposed in the end
            '''

            if d > CUTOFF:
                H = np.zeros((1,4))
                S = np.zeros((1,4))
                if atom_1.type == 'H' and atom_2.type == 'C':
                    return [H, S]
                else:
                    return [H.T, S.T]

            if d > self.C_maxdist:
                print('WARNING: distance between atoms longer than covered by DFTB parameters')
                H = np.zeros((1,4))
                S = np.zeros((1,4))
                if atom_1.type == 'H' and atom_2.type == 'C':
                    return [H, S]
                else:
                    return [H.T, S.T]

            # calculating where to look for right interactions
            i_loc = d / self.HC_dx
            i_high = int(i_loc)
            i_low = i_high - 1

            # print('i_low = {0}'.format(i_low))

            # factors for linear interpolation between gridpoints

            factor_low = i_loc - i_low
            factor_high = 1. - factor_low

            # grabbing the right values
            
            # hamiltonian
            H_ss_sigma = self.HC_ham_ss_sigma[i_low] * factor_low + self.HC_ham_ss_sigma[i_high] * factor_high
            H_sp_sigma = self.HC_ham_sp_sigma[i_low] * factor_low + self.HC_ham_sp_sigma[i_high] * factor_high
            # overlap
            S_ss_sigma = self.HC_ol_ss_sigma[i_low] * factor_low + self.HC_ol_ss_sigma[i_high] * factor_high
            S_sp_sigma = self.HC_ol_sp_sigma[i_low] * factor_low + self.HC_ol_sp_sigma[i_high] * factor_high

            '''
            order: ss, spx, spy, spz
            '''

            # H

            H_sp_table = [[H_sp_sigma * c for c in r]]

            # due to anti symmetry of p-orbitals we need to multiply
            # with -1 if integrals with p_i on the left side of the
            # bra-ket occur, as the tables only give < s | p >
            if atom_1.type == 'C' and atom_2.type == 'H':
                H_sp_table = np.array(H_sp_table) * (-1.)

            H = np.concatenate(([[H_ss_sigma]], H_sp_table), axis=1)

            # S

            S_sp_table = [[S_sp_sigma * c for c in r]]

            if atom_1.type == 'C' and atom_2.type == 'H':
                S_sp_table = np.array(S_sp_table) * (-1.)

            S = np.concatenate(([[S_ss_sigma]], S_sp_table), axis=1)


            # output

            if atom_1.type == 'H' and atom_2.type == 'C':
                return [H, S]
            else:
                return [H.T, S.T]


        else:
            print('ERROR: interaction for atoms type {0} and {1} not implemented yet'.format(atom_1, atom_2))

    #######################################
    # onsite energies
    #######################################

    def get_onsite_energies(self, atom):
        '''
        Gives onsite-energies and orbital interactions (all zero) of a single atom.
        '''
        if atom.type == 'C':
            onsite_list = [self.C_E_s, self.C_E_p, self.C_E_p, self.C_E_p]
            H = np.diag(onsite_list)
            S = np.identity(4)

            return [H, S]
        elif atom.type == 'H':
            onsite_list = [self.H_E_s]
            H = np.diag(onsite_list)
            S = np.identity(1)

            return [H, S]
        else:
            print('ERROR: onsite-energies for atoms type {0} not implemented yet'.format(atom.type))


#######################################
# onsite energies
#######################################

# class test
if __name__ == '__main__':
    targetdir = '../DFTB_CHNO/Hamilton/'

    p = ParameterSet(targetdir=targetdir)

    a_1 = Atom(type='H', coords=[.0, .0, .0])
    a_2 = Atom(type='C', coords=[1., 1., .0])

    H, S = p.get_interaction(a_1, a_2)

    H_2, S_2 = p.get_onsite_energies(a_1)
    H_3, S_3 = p.get_onsite_energies(a_2)


    print('H:\n{0}'.format(H))
    print('\nS:\n{0}'.format(S))

    print('\n\nH_2:\n{0}'.format(H_2))
    print('\nS_2:\n{0}'.format(S_2))


    M = np.block(
        [[H_2, H],
        [H.T, H_3]]
        )

    print('\n\n\n')
    for line in M:
        for x in line:
            print('{0:.3f}'.format(x), end='\t')
        print('\n')
