'''
class_vertice.py

Author: Tom Rodemund
Date: 21.07.2020

This is the class of one vertice in the network and contains methods to contract points

'''

import copy
import numpy as np

from constants import ETA

class Vertice:
    # constructors

    def __init__(self, center, atoms, vector):
        '''
        center_coords (ndarray, 3x1)... 3D coordinates of cell
        atom_coords (ndarray, Nx3)... 3D coordinates of atoms within cell
        '''
        self.name = 'TBD'

        self.center = center
        self.atoms = atoms
        self.vector = vector # vector where the next unit cell is to be placed

        self.hamiltonian = None
        self.overlap = None
        self.coupling = {}
        self.sigmas = {}


        self.is_contractable = True # only false for vertices at electrodes
        self.is_RDA_contractable = False

        # index of line, in which the vertice is
        self.line_index = None

    # properties

    @property
    def n_neighbors(self):
        return len(self.coupling)
    
    @property
    def n_atoms(self):
        return len(self.atoms)

    # methods

    def print(self):
        print('Vertice {0}'.format(self.name))
        print('\tcenter: {0}'.format(self.center))
        print('\tvector: {0}'.format(self.vector))
        print('\tline_index : {0}'.format(self.line_index))

        print('\tatoms ({0}):'.format(self.n_atoms))
        for a in self.atoms:
            print('\t\t{0} - {1}'.format(a.type, a.coords))
        
        print('\n\tneighbors ({0}):'.format(self.n_neighbors))
        for name in self.coupling.keys():
            if self.coupling[name] is None:
                print('\t\t- {0}:\t{1}'.format(name, self.coupling[name]))
            else:
                print('\t\t- {0}:\t{1}'.format(name, self.coupling[name][0]))
                for line in self.coupling[name][1:]:
                    print('\t\t\t{0}'.format(line))

        print('\n\thamiltonian:\t', end='')
        if self.hamiltonian is None:
            print(self.hamiltonian)
        else:
            print(self.hamiltonian[0])
            for line in self.hamiltonian[1:]:
                print('\t\t\t{0}'.format(line))

    def copy(self):
        return copy.deepcopy(self)