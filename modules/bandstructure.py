'''
Author: Tom Rodemund
Date: 02.09.2020

Calculates the 1D-bandstructure of the unit cell.
'''

import numpy as np
import scipy.linalg as la

from vertice_class import Vertice
from atom_class import Atom
from parameter_class import ParameterSet

import quantum_transport as qt
import plotting

def main():
    '''
    calculating ang plotting band structure
    '''

    cellpath = '../'
    savedir = '../results/'
    picdir = '../results/'
    DFTBdir = '../DFTB_CHNO/Hamilton/'

    print('reading DFTB parameters...')

    p = ParameterSet(DFTBdir)

    print('calculating bandstructure...')
    bandstructure = calculate_bandstructure(p, cellpath=cellpath, savedir=savedir)

    fermienergy = calculate_fermienergy(bandstructure, savedir)

    print('plotting...')
    plotting.bandstructure(bandstructure, savedir=picdir, fermienergy=fermienergy)


def print_matrix(M):
    for line in M:
        for x in line:
            print('{0:.3f}'.format(x), end='\t')
        print()


def calculate_bandstructure(p, cellpath='input/', savedir='results/'):
    '''
    calculate the bandstructure of the the unit cell in cellpath with
    DFTB parameterset p
    '''
    # n_atoms = np.loadtxt(cellpath, skiprows=0, max_rows=1, dtype=int)
    # vector = np.loadtxt(cellpath, skiprows=1, max_rows=1)

    path = cellpath + 'unit_cell.xyz'
    f = open(path, 'r')
    n_atoms = int(f.readline())
    vector = [float(x) for x in f.readline().split('\t')]
    f.close()


    # save atoms
    coords = np.loadtxt(path, usecols=(1,2,3,), skiprows=2)
    atom_types = np.genfromtxt(path, usecols=(0,), skip_header=2, dtype=str)

    if n_atoms == 1:
        coords = [coords]
        atom_types = [atom_types]

    atom_list = []
    for c, t in zip(coords, atom_types):
        a = Atom(type=t, coords=c)
        atom_list.append(a)

    center = np.array([.0, .0, .0])

    unit_cell = Vertice(center, atom_list, vector)

    # calculate hamiltonian of cell
    qt.set_hamiltonian(p, unit_cell)

    # define left and right cell
    right_cell = unit_cell.copy()
    right_cell.center = unit_cell.center + vector
    for a in right_cell.atoms:
        a.coords = a.coords + vector


    # calculate coupling
    qt.set_coupling(p, unit_cell, right_cell)

    # calculate band structure

    H0 = unit_cell.hamiltonian
    tau_R = unit_cell.coupling[right_cell.name]
    tau_L = tau_R.T


    S0 = unit_cell.overlap
    sigma_R = unit_cell.sigmas[right_cell.name]
    sigma_L = sigma_R.T

    # print('{0}\n\n{1}\n\n{2}'.format(H0, tau_R, tau_L))

    ka_list = np.linspace(-np.pi, np.pi, 1001)

    bandstructure = []

    for ka in ka_list:
        H = H0 + tau_R * np.exp(1j * ka) + tau_L * np.exp(- 1j * ka)
        S = S0 + sigma_R * np.exp(1j * ka) + sigma_L * np.exp(- 1j * ka)

        eigenvalues = np.sort([ev.real for ev in la.eigvals(a=H, b=S)])

        bandstructure.append(eigenvalues)

        # print('\n\nka = {0}\n{1}\n{2}'.format(ka, H, eigenvalues))

    # putting first row as x-values (ka)

    bandstructure = np.concatenate( [[ka_list], np.transpose(bandstructure)] )

    if savedir is not None:
        savepath = savedir + 'bandstructure.dat'
        np.savetxt(savepath, bandstructure)

    return bandstructure


def calculate_fermienergy(bandstructure, savedir='results/'):
    '''
    Every orbital can be occupied by 2 electrons, so we need to grab
    the middle energies.
    '''
    # first entry is left out as that is only the k-values
    energies = np.sort(np.concatenate(bandstructure[1:]))
    n_energies = len(energies)

    # uneven number of energies: there is an energy in the middle
    if n_energies % 2 == 1:
        # only need to devide as indizes start at 0
        index = int( n_energies/2 )
        fermienergy = energies[index]
    # even number of energies: two energies in the middle must be meaned
    else:
        index_upper = int( n_energies/2 )
        index_lower = index_upper - 1

        fermienergy = (energies[index_lower] + energies[index_upper]) * .5
    
    np.savetxt(savedir + 'fermienergy.dat', [fermienergy])

    return fermienergy

if __name__ == '__main__':
    main()