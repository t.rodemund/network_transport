'''
network_methods.py

Author: Tom Rodemund
Date: 21.07.2020

This file contains useful functions for the Network class.
'''

import random
import time
import numpy as np
import constants

A_CC = constants.A_CC
NEIGHBOR_CRIT = constants.CUTOFF


e0 = np.array([1., 0.])
def rotation_matrix_xy(theta):
    '''
    Creates a rotation Matrix for rotations in the x-y-plain.
    '''

    # parts of matrix
    a = np.cos(theta)
    b = np.sin(theta)

    M = [
        [a, -b, .0],
        [b, a, .0],
        [.0, .0, 1.]
        ]
    M = np.array(M)

    return M


def connect_neighbors_line(line_list):
    '''
    Connecting the vertices. Connecting the vertices in a line, then comparing vertices in a line to vertices
    in all other lines.
    '''

    # connections in line

    # the lines are ordered so we can iterate this way over the indices

    for line in line_list:

        n_linevertices = len(line)

        for i in range(n_linevertices-1):

            v_1 = line[i]
            v_2 = line[i+1]

            v_1.coupling[v_2.name] = None
            v_1.sigmas[v_2.name] = None

            v_2.coupling[v_1.name] = None
            v_2.sigmas[v_1.name] = None
        
    # connection to other lines

    n_lines = len(line_list)

    # iterating over lines

    for i in range(n_lines-1):

        line_1 = line_list[i]

        for j in range(i+1, n_lines):

            line_2 = line_list[j]

            # iterating over vertices

            for v_1 in line_1:
                for v_2 in line_2:
                    is_neighbor = False

                    for a_1 in v_1.atoms:
                        # as soon as two neighboring atoms are found
                        # all for loops are escaped
                        if is_neighbor:
                            break

                        c_1 = a_1.coords
                        for a_2 in v_2.atoms:
                            c_2 = a_2.coords
                            dist = np.linalg.norm(c_1 - c_2)

                            # as soon as two neighboring atoms are found
                            # all for loops are escaped
                            if dist < NEIGHBOR_CRIT:
                                is_neighbor = True
                                break
                    
                    if is_neighbor:
                        v_1.coupling[v_2.name] = None
                        v_1.sigmas[v_2.name] = None

                        v_2.coupling[v_1.name] = None
                        v_2.sigmas[v_1.name] = None
                    