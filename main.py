'''
main.py

Author: Tom Rodemund
Date: 20.07.2020


This file is the base of operations for the whole programm.

The steps needed to perform a calculation are:

1. read input file
2. construct the network
3. calculate transmission spectrum
4. calculate conductance
5. create graphics
'''

import sys
import random
import os
import time
import json
import numpy as np

sys.path.append('modules')

from network_class import Network
import plotting
import quantum_transport

def main():
    print('CALCULATING TRANSPORT PROPERTIES OF A NETWORK\n\n')

    time_total_start = time.time()
    calculation_time = .0


    ##########################################################
    # INPUT PARAMETERS
    ##########################################################

    print('READING INPUT FILE(S)...')

    input_file = open('input.json')
    parameters = json.load(input_file)

    inputdir = parameters['inputdir']
    outputdir = parameters['outputdir']
    DFTBdir = parameters['DFTBdir']

    length = parameters['length']
    width = parameters['width']
    n_lines = parameters['n_lines']
    linelength = parameters['linelength']
    layer_height = parameters['layer_height']

    seed = parameters['seed']

    fermi_energy = np.loadtxt(inputdir + 'fermienergy.dat')
    temperature = parameters['temperature']

    is_plotting = parameters['is_plotting']
    
    random.seed(seed)

    # creating network

    nw = Network()

    nw.load_DFTB(DFTBdir)
    nw.read_input(inputdir=inputdir)

    ##########################################################
    # INPUT PARAMETERS
    ##########################################################

    print('\nCONSTRUCTING NETWORK...')
    
    tries_max = 1000000
    n_tries = 0

    # calculating n_linelength

    len_vector = np.linalg.norm(nw.unit_cell.vector)
    n_linelength = int( round( linelength / len_vector ) )

    for i in range(tries_max):
        n_tries += 1
        print('\nTRY {0}'.format(n_tries))
        
        
        print('- create vertices')

        time_start = time.time()
        
        nw.create(length, width, n_lines, n_linelength, layer_height)

        time_end = time.time()
        time_needed = time_end - time_start
        calculation_time += time_needed
        displaytime(time_needed)

        if is_plotting:
            print('- plotting')
            nw.plot_vertices(mode='layers', title='grafics/network_step1.png')
            nw.plot_atoms(title='grafics/atoms_step1.png')



        print('- remove overhang')

        time_start = time.time()

        nw.remove_overhang()

        time_end = time.time()
        time_needed = time_end - time_start
        calculation_time += time_needed
        displaytime(time_needed)


        print('- finding neighbors')

        time_start = time.time()

        nw.initialize()

        time_end = time.time()
        time_needed = time_end - time_start
        calculation_time += time_needed
        displaytime(time_needed)


        if is_plotting:
            print('- plotting')
            nw.plot_vertices(mode='layers', title='grafics/network_step2.png')
            nw.plot_atoms(title='grafics/atoms_step2.png')



        print('- finding clusters')

        time_start = time.time()

        nw.find_clusters()

        time_end = time.time()
        time_needed = time_end - time_start
        calculation_time += time_needed
        displaytime(time_needed)


        if is_plotting:
            print('- plotting')
            nw.plot_vertices(mode='clusters', title='grafics/network_step3.png')



        print('- checking if percolating')

        time_start = time.time()

        is_percolating = nw.only_percolating()

        time_end = time.time()
        time_needed = time_end - time_start
        calculation_time += time_needed
        displaytime(time_needed)
        
        if is_percolating:
            print('\t-> percolating! :)')
            break
        else:
            print('\t-> NOT percolating! :(')
        
        
        # for testing purposes use only one try to see if everything works
        # break
    

    print('-> commencing!')
    
    np.savetxt(outputdir + 'n_tries.dat', [n_tries])

    if is_plotting:
        print('- plotting')
        nw.plot_vertices(mode='layers', title='grafics/network_step4.png')



    '''
    ATTENTION: this function isn't ised right now and only left in for potential 
    future use.

    print('- finding chains for RDS')

    time_start = time.time()

    nw.sep_RDA_lines()

    time_end = time.time()
    time_needed = time_end - time_start
    calculation_time += time_needed
    displaytime(time_needed)

    print('\t-> {0} RDS-steps necessary'.format(nw.RDA_steps_needed))
    
    if is_plotting:
        print('- plotting')
        nw.plot_vertices(mode='RDA_lines', title='grafics/network_step5.png')
    '''


    
    print('- calculating coupling of the vertices')
    
    time_start = time.time()

    nw.set_coupling()

    time_end = time.time()
    time_needed = time_end - time_start
    calculation_time += time_needed
    displaytime(time_needed)


    ##########################################################
    # CALCULATIONS
    ##########################################################

    # deleting old contraction graphics
    if is_plotting:
        directory = 'grafics/'
        all_files = os.listdir(directory)

        for f in all_files:
            split = f.split('_')

            if len(split) > 1:
                if split[1] == 'step7':
                    os.unlink(directory + f)


    print('\n- calculating transmission\n')

    delta_E = 1. # eV
    E_list = np.linspace(-delta_E * .5, delta_E * .5, 2001) + fermi_energy
    transmission_list = []

    time_start = time.time()

    for E in E_list:
        print('E = {0:.4f} eV'.format(E), end='\t')

        # ------ copy network ------

        nw_temp = nw.copy()

        # ------ calculation steps ------

        # corrections to matrices due to non-orthogonal basis
        # doesn't need to be made as traces of matrices do not
        # depend on the basis
        
        nw_temp.matrix_corrections(E)

        # RDA
        nw_temp.perform_RDA(E)

        

        if E == E_list[0] and is_plotting:
            nw_temp.plot_vertices(title='grafics/network_step6.png')
        
        # single contractions/RDS

        # contract n_vertices-2 as two vertices remain for FIS
        n_contractions = nw_temp.n_vertices - 2

        for i in range(n_contractions):
            # print('\tcontraction {0}/{1}'.format(i+1, n_contractions))

            nw_temp.contract_single(E)

            # all plots are created for one energy only, as they are the same
            # for all other energies
            if is_plotting and E == E_list[0]:
                nw_temp.plot_vertices(title='grafics/network_step7_contr{0:04d}.png'.format(i+1))

        # transmission

        transmission = nw_temp.perform_FIS(E)
        
        print('\tT = {0:.6f}'.format(transmission))

        transmission_list.append(transmission)

    # rescaling of E_list to E_f
    E_list = E_list - fermi_energy
    data = np.array([E_list, transmission_list])

    np.savetxt(outputdir + 'transmission.dat', [E_list, transmission_list])

    time_end = time.time()
    time_needed = time_end - time_start
    calculation_time += time_needed
    displaytime(time_needed)

    # if is_plotting:
    if is_plotting:
        plotting.transmission(data, savedir=outputdir)




    print('\n- calculating conductance')
    
    time_start = time.time()

    G = quantum_transport.calc_conductivity(E_list, transmission_list, temperature)

    time_end = time.time()
    time_needed = time_end - time_start
    calculation_time += time_needed
    displaytime(time_needed)

    print('\tG/G0 = {0:.6f}'.format(G))

    np.savetxt(outputdir + 'conductivity.dat', [G])




    ##########################################################
    # END
    ##########################################################

    print('\n\nDONE!\n')

    time_total_end = time.time()
    time_total_needed = time_total_end - time_total_start

    np.savetxt('results/calculation_time.dat', [time_total_needed])

    print('total time:', end='\t\t')
    displaytime(time_total_needed, tabs=0)

    print('calculation time:', end='\t')
    displaytime(calculation_time, tabs=0)

    


def print_matrix(M):
    for line in M:
        for x in line:
            print('{0:.3f}'.format(x), end='\t')
        print()


def displaytime(t, tabs=1):
    '''
    Displays the time in a formatted way.
    '''

    # total time in ms
    total = int(round( t * 1000. ))

    n_h = int( total / 3600000 )
    n_m = int( total % 3600000 / 60000 )
    n_s = int( total % 60000 / 1000 )
    n_ms = total % 1000

    # display ms
    if total < 1000:
        time_string = '{0} ms'.format(n_ms)
    # up to seconds
    elif total < 60000:
        time_string = '{0} s, {1} ms'.format(n_s, n_ms)
    # up to minutes
    elif total < 3600000:
        time_string = '{0} m, {1} s, {2} ms'.format(n_m, n_s, n_ms)
    # up to hours
    else:
        time_string = '{0} h, {1} m, {2} s, {3} ms'.format(n_h, n_m, n_s, n_ms)

    print('\t' * tabs + time_string)

if __name__ == '__main__':
    main()