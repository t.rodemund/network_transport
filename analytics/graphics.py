import numpy as np

import matplotlib.pyplot as plt

def main():
    # percolation graphic

    print('percolation graphic')
    
    fig, ax = plt.subplots()

    n_lines_list, n_percolation_percentage_list = np.loadtxt('result_percolation_percentage.dat')
    
    ax.plot(n_lines_list, n_percolation_percentage_list)
    
    ax.set_xlim(0, n_lines_list[-1])
    
    ax.set_xlabel('n_lines')
    ax.set_ylabel('percolation percentage [%]')
    
    ax.tick_params(
            which='both',
            direction='in'
            )
    
    fig.savefig('result_percolation_percentage.png', dpi=300, bbox_inches='tight')
    plt.close()
    
    # time graphic
    
    print('time graphic')
    
    fig, ax = plt.subplots()

    n_lines_list, time_needed_list = np.loadtxt('result_calculation_time.dat')
    
    ax.plot(n_lines_list, np.cbrt(time_needed_list))
    
    ax.set_xlim(0, n_lines_list[-1])
    
    ax.set_xlabel('n_lines')
    ax.set_ylabel('cbrt(calculation time) [cbrt(s)]')
    
    ax.tick_params(
            which='both',
            direction='in'
            )
    
    fig.savefig('result_calculation_time.png', dpi=300, bbox_inches='tight')
    plt.close()

if __name__ == '__main__':
    main()