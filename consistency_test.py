'''
Comparing the results between the trivial approach (network as one single cell in the center) and the network approach.

For simplicity, a linear chain of is used
'''


import sys
import random
import os
import time
import json
import numpy as np

import matplotlib.pyplot as plt

sys.path.append('modules')

from network_class import Network
import plotting
import quantum_transport
from constants import ETA


def main():
    print('TESTING CONSISTENCY OF NETWORK APPROACH\n\n')

    time_total_start = time.time()
    calculation_time = .0


    ##########################################################
    # INPUT PARAMETERS
    ##########################################################

    print('READING INPUT FILE(S)...')

    input_file = open('input.json')
    parameters = json.load(input_file)

    inputdir = parameters['inputdir']
    outputdir = parameters['outputdir']
    DFTBdir = parameters['DFTBdir']

    length = parameters['length']
    width = parameters['width']
    n_lines = parameters['n_lines']
    linelength = parameters['linelength']
    layer_height = parameters['layer_height']

    seed = parameters['seed']

    fermi_energy = np.loadtxt(inputdir + 'fermienergy.dat')
    temperature = parameters['temperature']

    is_plotting = parameters['is_plotting']
    
    random.seed(seed)

    # creating network

    nw = Network()

    nw.load_DFTB(DFTBdir)
    nw.read_input(inputdir=inputdir)

    ##########################################################
    # INPUT PARAMETERS
    ##########################################################

    print('\nCONSTRUCTING NETWORK...')
    
    tries_max = 1000000
    n_tries = 0

    # calculating n_linelength

    len_vector = np.linalg.norm(nw.unit_cell.vector)
    n_linelength = int( round( linelength / len_vector ) )

    for i in range(tries_max):
        n_tries += 1
        print('\nTRY {0}'.format(n_tries))
        
        
        print('- create vertices')

        nw.create(length, width, n_lines, n_linelength, layer_height)



        print('- remove overhang')

        nw.remove_overhang()


        print('- finding neighbors')

        nw.initialize()


        print('- finding clusters')


        nw.find_clusters()


        print('- checking if percolating')

        is_percolating = nw.only_percolating()

        
        if is_percolating:
            print('\t-> percolating! :)')
            break
        else:
            print('\t-> NOT percolating! :(')
        
        
        # for testing purposes use only one try to see if everything works
        # break
    

    print('-> commencing!')
    
    np.savetxt(outputdir + 'consistency-n_tries.dat', [n_tries])


    print('- calculating coupling of the vertices')

    nw.set_coupling()



    print('\n- calculating transmission\n')

    delta_E = 1. # eV
    E_list = np.linspace(-delta_E * .5, delta_E * .5, 2001) + fermi_energy
    # E_list = np.array([.1])

    E_list_out = E_list - fermi_energy



    print('\t=> network approach')

    transmission_list_network = []

    time_network_start = time.time()

    for E in E_list:
        print('\t\tE = {0:.4f} eV'.format(E), end='\t')

        # ------ copy network ------

        nw_temp = nw.copy()

        # ------ calculation steps ------

        # corrections to matrices due to non-orthogonal basis
        # doesn't need to be made as traces of matrices do not
        # depend on the basis
        
        nw_temp.matrix_corrections(E)

        # RDA
        nw_temp.perform_RDA(E)
        
        # single contractions/RDS

        # contract n_vertices-2 as two vertices remain for FIS
        n_contractions = nw_temp.n_vertices - 2

        for i in range(n_contractions):
            # print('\tcontraction {0}/{1}'.format(i+1, n_contractions))

            nw_temp.contract_single(E)

        # transmission

        transmission = nw_temp.perform_FIS(E)
        
        print('\tT = {0:.6f}'.format(transmission))

        transmission_list_network.append(transmission)

    # saving time

    time_network_end = time.time()
    time_network = time_network_end - time_network_start

    np.savetxt(outputdir + 'consistency-network_time.dat', [time_network])

    # saving transmission

    transmission_list_network = np.array(transmission_list_network)
    data_network = np.array([E_list_out, transmission_list_network])

    np.savetxt(outputdir + 'consistency-network_transmission.dat', data_network)



    print('\t=> trivial approach')

    transmission_list_trivial = []

    # map matrix indizes to vertices
    indizes = range(nw.n_vertices)

    name_to_index = {}
    for i, key in zip(indizes, nw.vertice_list.keys()):
        name_to_index[key] = i
    

    # assuming all cells have the same number of orbitals
    orbitals_per_cell = 0
    v_example = list(nw.vertice_list.values())[0]
    for atom in v_example.atoms:
        a_type = atom.type
        if a_type == 'C':
            orbitals_per_cell += 4
        elif a_type == 'H':
            orbitals_per_cell += 1
        else:
            print('WARNING: atom type {0} not defined'.format(a_type))

    # print(name_to_index)

    time_trivial_start = time.time()

    for E in E_list:
        print('\t\tE = {0:.4f} eV'.format(E), end='\t')

        # ------ copy network ------

        nw_temp = nw.copy()

        # ------ calculation steps ------

        # corrections to matrices due to non-orthogonal basis
        # doesn't need to be made as traces of matrices do not
        # depend on the basis
        
        nw_temp.matrix_corrections(E)

        # RDA
        nw_temp.perform_RDA(E)

        # constructing hamiltonian of center part

        M_dim = nw_temp.n_vertices
        M = empty_block_matrix(M_dim, orbitals_per_cell)

        for v in nw_temp.vertice_list.values():
            # continue here
            i = name_to_index[v.name]
            M[i][i] = v.hamiltonian

            for key in v.coupling.keys():
                j = name_to_index[key]
                tau = v.coupling[key]

                M[i][j] = tau
        

        # print(M[0])

        # print(M)
        M = np.block(M)
        
        '''
        for line in M:
            for x in line:
                if np.abs(x) < 10**(-3):
                    x_out = 1
                else:
                    x_out = 0
                print('{0:.0f}'.format(x_out), end='\t')
            print('')
        '''

        # check if M hermitian
        norm = np.linalg.norm(M - M.T)
        
        if norm > 10**(-3):
            print('WARNING: H_C not hermitian', end='\t')


        # constructing broadening matrices

        electrode_left, electrode_right = nw_temp.electrodes
        gamma_left, gamma_right = nw_temp.gamma_list
        # print('\n', gamma_left)

        gamma_left_big = empty_block_matrix(M_dim, orbitals_per_cell)
        i_left = name_to_index[electrode_left.name]
        gamma_left_big[i_left][i_left] = gamma_left
        gamma_left_big = np.block(gamma_left_big)

        # print(gamma_left_big)

        gamma_right_big = empty_block_matrix(M_dim, orbitals_per_cell)
        i_right = name_to_index[electrode_right.name]
        gamma_right_big[i_right][i_right] = gamma_right
        gamma_right_big = np.block(gamma_right_big)

        # calculating green's function of channel
        G = np.linalg.inv( (E + ETA) * np.identity(M.shape[0]) - M )

        # calculating transmission
        transmission = np.trace(gamma_right_big @ G @ gamma_left_big @ G.conj().T)
        transmission = transmission.real



        print('\tT = {0:.6f}'.format(transmission))

        transmission_list_trivial.append(transmission)
    
    # saving time

    time_trivial_end = time.time()
    time_trivial = time_trivial_end - time_trivial_start

    np.savetxt(outputdir + 'consistency-trivial_time.dat', [time_trivial])

    # saving transmission

    transmission_list_trivial = np.array(transmission_list_trivial)
    data_trivial = np.array([E_list_out, transmission_list_trivial])

    np.savetxt(outputdir + 'consistency-trivial_transmission.dat', data_trivial)


    # comparisons

    print('\ncomparing approaches')

    print('\tdifference norm: ', np.linalg.norm(transmission_list_network - transmission_list_trivial))

    print('\tnetwork: {0:.2f} s'.format(time_network))
    print('\ttrivial: {0:.2f} s'.format(time_trivial))








def plot_test():
    '''
    plotting and comparing the transmission curves
    '''

    print('PLOTTING CONSISTENCY TEST')

    # reading data

    print('reading data...')

    data_network = np.loadtxt('results/consistency-network_transmission.dat')
    data_trivial = np.loadtxt('results/consistency-trivial_transmission.dat')

    # plotting

    print('plotting...')

    fig, ax = plt.subplots(figsize=(5,3))

    ax.semilogy(*data_trivial, label='trivial', linewidth=5.)
    ax.semilogy(*data_network, linestyle='dashed', label='network', linewidth=3.)

    # cosmetics

    ax.set_xlim(data_trivial[0,0], data_trivial[0,-1])

    ax.legend(title='method')

    ax.set_xlabel(r'$E - E_\mathrm{F}$ [eV]')
    ax.set_ylabel(r'transmission')

    # saving

    fig.savefig('results/consistency.png', dpi=300, bbox_inches='tight')
    plt.close()


def empty_block_matrix(dim, subdim):
    return [[np.zeros((subdim,subdim)) for i in range(dim)] for j in range(dim)]

if __name__ == '__main__':
    # main()
    plot_test()